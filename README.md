# README #

 **AndroidSQlite**  is a SQLite Demo app developed in Android to store the data in local database

### What is this repository for? ###

This repository is for Android SignOn assignment to store data in local database

### How do I get set up? ###

1.	Import project into Android Studio

2.	Open the SDK manager to install the required Android SDK Tools and Android SDK Build-tools.

3.	There are no Third party libraries or Gradle dependencies used.

4.	minSdkVersion 14 targetSdkVersion 23

5.	Build the Project.

6.	Compile and Run


**What does the App do?**

•	Allows the user to Add or Delete a set of previously stored data

•	Data is stored in a local database

•	Data is displayed in chronological order


**Configuration**- Android Studio IDE, SQLite, Genymotion (Simulator)