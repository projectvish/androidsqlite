package com.projectvish.androidsqlite;

/**
 * Created by Vish on 9/12/15.
 */
public class Comment {
    private long id;
    private String comment;

    public long getId(){
        return id;
    }
    public void setId(long id){
        this.id = id;
    }
    public String getComment(){
        return comment;
    }
    public void setComment(String comment){
        this.comment = comment;
    }

    //Will be used by the ArrayAdapter in ListView
    @Override
    public String toString(){
        return comment;
    }

}
